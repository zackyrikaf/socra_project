package com.epita.socra.app;

import com.epita.socra.app.tools.Iteration_1;
import com.epita.socra.app.tools.Iteration_2;
import org.junit.Test;

import static com.epita.socra.app.tools.Iteration_1.convert;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.epita.socra.app.tools.IOAdapter;

/**
 * Unit test for simple App.
 */
public class AppTest {
    /**
     * Rigorous Test.
     */
    @Test
    public void givenAMock_WhenRunningMain_ThenCheckOuputs() {
        IOAdapter mock = mock(IOAdapter.class);
        when(mock.read()).thenReturn("TEST");
        App app = new App(mock);
        app.run();

        verify(mock).write("Hello, what's your name ?");
        verify(mock).write(argThat(message -> message.contains("TEST")));
    }
    Iteration_1 obj;
    Iteration_2 obj_2;

    @Test
    public void test_medium_number_35() {
        assert convert("35").equals("...-- ..... ");
    }

    @Test
    public void test_medium_number_59() {
        assert convert("59").equals("..... ----. ");
    }

    @Test
    public void test_medium_number_14() {
        assert convert("14").equals(".---- ....- ");
    }

    @Test
    public void test_medium_number_996() {
        assert convert("996").equals("----. ----. -.... ");
    }


    @Test
    public void test_medium_number_187_tomorse()
    {
        assert obj.convert("187").equals(".---- ---.. --... ");
    }

    @Test
    public void test_0_tomorse()
    {
        assertEquals("----- ", obj.convert("0"));
    }

    @Test
    public void test_1_tomorse()
    {
        assertEquals(".---- ", obj.convert("1"));
    }

    @Test
    public void test_2_tomorse()
    {
        assertEquals("..--- ", obj.convert("2"));
    }

    @Test
    public void test_3_tomorse()
    {
        assertEquals("...-- ", obj.convert("3"));
    }

    @Test
    public void test_4_tomorse()
    {
        assertEquals("....- ", obj.convert("4"));
    }

    @Test
    public void test_5_tomorse()
    {
        assertEquals("..... ", obj.convert("5"));
    }
    @Test
    public void test_6_tomorse()
    {
        assertEquals("-.... ", obj.convert("6"));
    }

    @Test
    public void test_7_tomorse()
    {
        assertEquals("--... ", obj.convert("7"));
    }

    @Test
    public void test_8_tomorse()
    {
        assertEquals("---.. ", obj.convert("8"));
    }

    @Test
    public void Hard_test_to_morse_123456789()
    {
        assertEquals(".---- ..--- ...-- ....- ..... -.... --... ---.. ----. ", obj.convert("123456789"));
    }

    @Test
    public void test_big_number() {
        assert obj.convert("1219").equals(".---- ..--- .---- ----. ");
        assert obj.convert("5369").equals("..... ...-- -.... ----. ");
        assert obj.convert("9854").equals("----. ---.. ..... ....- ");
    }

    @Test
    public void arabian2morse_with_9()
    {
        assertEquals("9", obj_2.convert("----."));
    }

    @Test
    public void arabian2morse_with_8()
    {
        assertEquals("8", obj_2.convert("---.."));
    }

    @Test
    public void arabian2morse_with_7()
    {
        assertEquals("7", obj_2.convert("--..."));
    }@Test
    public void arabian2morse_with_6()
    {
        assertEquals("6", obj_2.convert("-...."));
    }@Test
    public void arabian2morse_with_5()
    {
        assertEquals("5", obj_2.convert("....."));
    }@Test
    public void arabian2morse_with_4()
    {
        assertEquals("4", obj_2.convert("....-"));
    }@Test
    public void arabian2morse_with_3()
    {
        assertEquals("3", obj_2.convert("...--"));
    }@Test
    public void arabian2morse_with_2()
    {
        assertEquals("2", obj_2.convert("..---"));
    }@Test
    public void arabian2morse_with_1()
    {
        assertEquals("1", obj_2.convert(".----"));
    }@Test
    public void arabian2morse_with_0()
    {
        assertEquals("0", obj_2.convert("-----"));
    }@Test
    public void arabian2morse_with_123456789()
    {
        assertEquals("123456789", obj_2.convert(".----..---...--....-.....-....--...---..----."));
    }


}
