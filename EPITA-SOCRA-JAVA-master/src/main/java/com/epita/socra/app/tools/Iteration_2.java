package com.epita.socra.app.tools;

public class Iteration_2 {

    static String find_val(String str){ //throws IllegalArgumentException {

        if (str.equals("-----"))
            return "0";
        if (str.equals(".----"))
            return "1";
        if (str.equals("..---"))
            return "2";
        if (str.equals("...--"))
            return "3";
        if (str.equals("....-"))
            return "4";
        if (str.equals("....."))
            return "5";
        if (str.equals("-...."))
            return "6";
        if (str.equals("--..."))
            return "7";
        if (str.equals("---.."))
            return "8";
        if (str.equals("----."))
            return "9";
        System.out.print(str);
        return str;
        //throw new IllegalArgumentException("Bad argument !");
    }

    public static String convert(String morse)
    {
        String res = "";

        for (Integer i = 0; i < morse.length(); i+=5)
        {
            String mo = "";
            for (Integer j = i; j < i + 5; j++)
            {
                char s = morse.charAt(j);
                mo += s;
            }
            res += find_val(mo);
        }
        return res;
    }

    public static void main(String[] args) {
        System.out.println(convert("---..----.----."));
    }
}
