package com.epita.socra.app.tools;

public class Iteration_1 {
    static String find_val(char c ) throws IllegalArgumentException
    {
        switch (c)
        {
            case '0':
                return "-----";
            case '1':
                return ".----";
            case '2':
                return "..---";
            case '3':
                return "...--";
            case '4':
                return "....-";
            case '5':
                return ".....";
            case '6':
                return "-....";
            case '7':
                return "--...";
            case '8':
                return "---..";
            case '9':
                return "----.";
        }
        throw new IllegalArgumentException("Bad argument !");
    }
    public static String convert(String number)
    {
        Integer my_int;
        String res = "";
        for (Integer i = 0; i < number.length(); i++)
        {
            char s = number.charAt(i);
            res += find_val(s) + " ";
        }
        return res;
    }

    public static void main(String[] args) {
        System.out.println(convert("12"));
    }
}


